//Mady Fouert 2035774
package code;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class TestSphere {

    @Test
    public void testGetRadius() 
    {
        Sphere s = new Sphere(5.0);
        assertEquals(5.0, s.getRadius(), 0.01);
    }

    @Test
    public void testValidation() {
 
        Throwable exception = assertThrows(
            IllegalArgumentException.class, () -> {
                Sphere s = new Sphere(-6);
                s.getRadius();
            }
        );
        assertEquals("Negative Value Invalid", exception.getMessage());
    }

    @Test
    public void testGetVolume() 
    {
        Sphere s = new Sphere(5.0);
        assertEquals(523.60, s.getVolume(), 0.01);
    }

    @Test
    public void testGetSurfaceArea()
    {
        Sphere s = new Sphere(5.0);
        assertEquals(314.16, s.getSurfaceArea(), 0.01);
    }
}

