//Katharina Orfanidis 1842297
package code;
public class Sphere {
    private double radius;

    public Sphere(double radius){
        //constructor with data validation for radius
        if(radius < 0){
            throw new IllegalArgumentException("Negative Value Invalid");
        }
        this.radius = radius;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getVolume(){
        //should return π*radius^3
        return (4.0 / 3.0) * Math.PI * (Math.pow(this.radius, 3));
    }

    public double getSurfaceArea(){
        //should return 4*π*〖radius〗^2
        return (4 * Math.PI ) * (Math.pow(this.radius, 2));
    }
}
