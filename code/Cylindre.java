package code;
// Mady Fouert 2035774
public class Cylindre {
    private double height;
    private double radius; 

    public Cylindre(double height, double radius)
    {
        if(height < 0 || radius < 0){
            throw new IllegalArgumentException("Negative Value Invalid");
        }
        this.height = height;
        this.radius = radius;
    }
    
    public double getHeight()
    {
        return this.height;
    }

    public double getRadius()
    {
        return this.radius;
    }

    public double getVolume()
    {
        double volume = ((Math.PI * Math.pow(this.radius, 2)) * height);
        //calculates and returns the volume  π*radius^2*height
        return volume;
    }

    public double getSurfaceArea()
    {
        double surface = ((2 * Math.PI) * Math.pow(this.radius, 2) + 2 * Math.PI * this.radius * this.height);
        //calculates and returns the surface area 2π*radius^2+2* π*radius*height
        return surface;
    }
    
}
