//Katharina Orfanidis 1842297
package code;
public class SphereSkeleton {

    private double radius;

    public SphereSkeleton(double radius){
        //constructor with data validation for radius
    }

    public double getRadius(){
        return this.radius;
    }

    public double getVolume(){
        //should return π*radius^3
        return 5;
    }

    public double getSurfaceArea(){
        //should return 4*π*〖radius〗^2
        return 2.5;
    }
}