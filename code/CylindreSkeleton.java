package code;
//Mady Fouert 2035774
public class CylindreSkeleton {
    private double height;
    private double radius; 

    public CylindreSkeleton(double height, double radius)
    {
        
    }
    
    public double getHeight()
    {
        return this.height;
    }

    public double getRadius()
    {
        return this.radius;
    }

    public double getVolume()
    {
        //calculates and returns the volume  π*radius^2*height
        return 3;
    }

    public double getSurfaceArea()
    {
        //calculates and returns the surface area 2π*radius^2+2* π*radius*height
        return 4.8;
    }
}
