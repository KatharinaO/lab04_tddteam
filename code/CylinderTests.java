//Katharina Orfanidis 1842297

package code;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class CylinderTests {

    @Test
    public void testGetters() {
        Cylindre c = new Cylindre(5, 3);
        assertEquals(5, c.getHeight());
        assertEquals(3, c.getRadius());
    }

    @Test
    public void testNegativeRadiusValidation() {
 
        Throwable exception = assertThrows(
            IllegalArgumentException.class, () -> {
                Cylindre c = new Cylindre(-6, 5);
                c.getRadius();
            }
        );
        assertEquals("Negative Value Invalid", exception.getMessage());
    }

    @Test
    public void testNegativeHeightValidation() {
 
        Throwable exception = assertThrows(
            IllegalArgumentException.class, () -> {
                Cylindre c = new Cylindre(6, -5);
                c.getRadius();
            }
        );
        assertEquals("Negative Value Invalid", exception.getMessage());
    }

    @Test
    public void testNegativeBothValidation() {
 
        Throwable exception = assertThrows(
            IllegalArgumentException.class, () -> {
                Cylindre c = new Cylindre(-6, -5);
                c.getRadius();
            }
        );
        assertEquals("Negative Value Invalid", exception.getMessage());
    }


    @Test 
    public void testGetVolume() {
        Cylindre c = new Cylindre(5, 3);
        assertEquals(141.37, c.getVolume(), 0.01);
    }

    @Test
    public void testGetSurfaceArea() {
        Cylindre c = new Cylindre(5, 3);
        assertEquals(150.8, c.getSurfaceArea(), 0.01);
    }
}
